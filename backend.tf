terraform {
  backend "http" {
    address  = "https://gitlab.com/api/v4/projects/25271378/terraform/state/terraform.tfstate"
    username = "sundowndev1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.32"
    }
  }
}
